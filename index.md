# Przygotowanie Środowiska

Witaj na warsztatach `Technical Architecture and Patterns`. 

W tej sekcji dowiesz się jak skonfigurować swoje środowisko developerskie.

## Oprogramowanie

- IDE (jedno z):
  - [Visual Studio Community Edition](https://visualstudio.microsoft.com/pl/downloads/)
  - [Rider](https://www.jetbrains.com/rider/) (Narzędzie płatne) <Badge text="Wybór prowadzącego"/>
  - [Visual Studio Code](https://code.visualstudio.com/download)
- [Postman](https://www.getpostman.com/)
- Przeglądarka internetowa (Chrome / Firefox / Brave)
- [Git](https://git-scm.com/)
- Terminal
  - Git Bash - Jest instalowany razem z Git-em
  - [Cmder](https://cmder.net/) - trochę bardziej zaawansowana powłoka dla systemu operacyjnego windows
  - [ITerm2](https://iterm2.com/) - trochę bardziej zaawansowany terminal dla systemu Mac Os X <Badge text="Wybór prowadzącego"/>
- Notatnik markdown:
  - [Visual Studio Code](https://code.visualstudio.com/download)
  - [Obsidian](https://obsidian.md/) <Badge text="Wybór prowadzącego"/>

### Odblokowany dostep do domen:

- [https://miro.com/]
- [https://egon.io/]
- [https://www.canva.com]

## Technologie

| Nazwa | Wersja | Pliki instalacyjne |
| --- | -- | -- |
| .NET | 7+ | [https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download) |
| Node | 18.17+ | [https://nodejs.org/en](https://nodejs.org/en) |
| npm | 9.6+ | [https://nodejs.org/en](https://nodejs.org/en) |
| docker | 24.0 + | [https://docs.docker.com/desktop/](https://docs.docker.com/desktop/) |


### Postman
Podczas warsztatów bedą wykonywane szyfrowane połaczenia lokalne (https i wss). Koniecznym jest skonfigurowanie Postman-a, aby nie walidował certyfikatów SSL. Wystarczy wejsć w ustawienia Postman-a i odznacz zaznaczoną na poniższym obrazku opcję.

![An image](https://linecode.pl/workshops/assets/postman.png)
