# Literature

### Bibliografia

- Design IT - Michael Keeling: https://www.goodreads.com/book/show/31670678-design-it
- The Almanack of Naval Ravikant: https://www.goodreads.com/book/show/54898389-the-almanack-of-naval-ravikant
- Event Storming - Alberto Brandolini: https://www.goodreads.com/book/show/49819779-event-storming
- Building Evolutionary Architectures: Support Constant Change - Neal Ford, Rebecca PArsons, Patrick Kua: https://www.goodreads.com/book/show/35755822-building-evolutionary-architectures
- BABOK. A Guide to Business Analysis Body of Knowledge - International Institute of Business Analysis: https://www.goodreads.com/book/show/39674082-babok-a-guide-to-business-analysis-body-of-knowledge
- Hands-On Domain-Driven Design with .NET Core: Tackling complexity in the heart of software by putting DDD principles into practice - Alexey Zimarev: https://www.goodreads.com/book/show/40219648-hands-on-domain-driven-design-with-net-core
- Learning Domain-Driven Design: Aligning Software Architecture and Business Strategy - Vladik Khononov: https://www.goodreads.com/book/show/57573212-learning-domain-driven-design
- Domain Storytelling: A Collaborative, Visual, and Agile Way to Build Domain-Driven Software (Addison-Wesley Signature Series - Henning Schwentner, Stefan Hofer: https://www.goodreads.com/book/show/58385794-domain-storytelling
- User Story Mapping: Discover the Whole Story, Build the Right Product - Jeff Patton, Peter Economy: https://www.goodreads.com/book/show/22221112-user-story-mapping
- Impact Mapping: Making a Big Impact with Software Products and Projects - Gojko Adzic: https://www.goodreads.com/book/show/16084015-impact-mapping
- Implementing Domain-Driven Design - Vaughn Vernon: https://www.goodreads.com/book/show/15756865-implementing-domain-driven-design
- Analysis Patterns: Reusable Object Models - Martin Fowler: https://www.goodreads.com/book/show/85002.Analysis_Patterns
- Enterprise Patterns and MDA: Building Better Software with Archetype Patterns and UML - Jim Arlow, Ila Neustadt: https://www.goodreads.com/book/show/434826.Enterprise_Patterns_and_MDA
- Cynefin - Weaving Sense-Making into the Fabric of Our World - Dave Snowden, Zhen Goh, Sue Borchardt (Illustrator)), Riva Greenberg (Editor), Boudewijn Bertsch (Editor), Sonja Blignaut (Introduction): https://www.goodreads.com/book/show/55813487-cynefin---weaving-sense-making-into-the-fabric-of-our-world
- System Design Interview – An insider's guide - Alex Hu: https://www.goodreads.com/book/show/54109255-system-design-interview-an-insider-s-guide
- System Design Interview – An Insider's Guide: Volume 2 - Alex Hu, Sahn Lam: https://www.goodreads.com/book/show/60631342-system-design-interview-an-insider-s-guide
- Just Enough Software Architecture: A Risk-Driven Approach - George H. Fairbanks: https://www.goodreads.com/book/show/9005772-just-enough-software-architecture
- The Mythical Man-Month: Essays on Software Engineering - Frederick P. Brooks Jr.: https://www.goodreads.com/book/show/13629.The_Mythical_Man_Month
- Software Design X-Rays: Fix Technical Debt with Behavioral Code Analysis - Adam Tonhill: https://www.goodreads.com/book/show/36517037-software-design-x-rays
- Team Topologies: Organizing Business and Technology Teams for Fast Flow - Matthew Skelton, Manuel Pais: https://www.goodreads.com/book/show/44135420-team-topologies
- The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations - Gene Kim, Patrick Debois, John Willis, Jez Humble: https://www.goodreads.com/book/show/26083308-the-devops-handbook
- Escaping the Build Trap: How Effective Product Management Creates Real Value - Melissa Perri: https://www.goodreads.com/book/show/42611483-escaping-the-build-trap
- The Phoenix Project: A Novel About IT, DevOps, and Helping Your Business Win - Gene Kim, Kevin Behr, George Spafford: https://www.goodreads.com/book/show/17255186-the-phoenix-project
- Software Architecture Patterns - Mark Richards: https://www.goodreads.com/book/show/25091671-software-architecture-patterns
- Strategic Monoliths and Microservices: Driving Innovation Using Purposeful Architecture - Vaughn Vernon, Tomasz Jaskula: https://www.goodreads.com/book/show/55782292-strategic-monoliths-and-microservices
- Domain-Driven Design: Tackling Complexity in the Heart of Software - Eric Evans: https://www.goodreads.com/book/show/179133.Domain_Driven_Design
- Object Design: Roles, Responsibilities, and Collaborations - Rebecca Wirfs-Brock, Alan McKean: https://www.goodreads.com/book/show/179204.Object_Design
- Building Microservices: Designing Fine-Grained Systems - Sam Newman: https://www.goodreads.com/book/show/22512931-building-microservices
- Patterns of Enterprise Application Architecture - Martin Fowler: https://www.goodreads.com/book/show/70156.Patterns_of_Enterprise_Application_Architecture
- Clean Architecture - Robert C. Martin: https://www.goodreads.com/book/show/18043011-clean-architecture
- Release It!: Design and Deploy Production-Ready Software - Michael T. Nygard: https://www.goodreads.com/book/show/1069827.Release_It_
- Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions - Gregor Hohpe, Bobby Woofl: https://www.goodreads.com/book/show/85012.Enterprise_Integration_Patterns
- Modern Enterprise Architecture: Using DevSecOps and Cloud-Native in Large Enterprises - Jeroen Mulder: https://www.goodreads.com/book/show/79699555-modern-enterprise-architecture
- Everyday Enterprise Architecture: Sense-making, Strategy, Structures, and Solutions - Tom Graves: https://www.goodreads.com/book/show/75270137-everyday-enterprise-architecture

### Links

- https://github.com/ddd-crew/ddd-starter-modelling-process
- https://domainlanguage.com/ddd/whirpool
- https://miro.com/miroverse/c4-architecture/
- https://mermaid.js.org/syntax/c4.html
- https://github.com/plantuml-stdlib/C4-PlantUML
- https://structurizr.com/
- https://stackexchange.com/performance
- https://12factor.net/
- https://www.youtube.com/watch?v=w9YhmMPLQ4U
- https://codescene.io/projects/168/jobs/559117/results?scope=month#code-health
- https://architectelevator.com/cloud/hybrid-multi-cloud/
- https://microservices.io/patterns/apigateway.html
- https://microservices.io/patterns/data/api-composition.html
- https://digitalvarys.com/what-is-circuit-breaker-design-pattern/
- https://www.youtube.com/watch?v=NZ5mI6tNUc