# Materiały

## Prezentacje

- [Część 1](https://www.canva.com/design/DAForrUX7xw/DSho2_3WAgSB2bpwC7haFg/view?utm_content=DAForrUX7xw&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)
- [Część 2](https://www.canva.com/design/DAFqaelfFnk/0APd2UIq2ORE-2Xkx5aC-w/view?utm_content=DAFqaelfFnk&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)
- [Część 3](https://www.canva.com/design/DAFqtWLhQiY/IqTPXyB9Q8-SvUw-MsIROw/view?utm_content=DAFqtWLhQiY&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)

## Zadania

- [Workbook](https://www.canva.com/design/DAFqssB-3is/wZGCqvZWqVOmH7CkeeC8Tw/view?utm_content=DAFqssB-3is&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)

## Miro

- [Stakeholder Mapping](https://miro.com/app/board/uXjVMwF_wUU=/)
- [Event Storming](https://miro.com/app/board/uXjVMwFE2Ak=/)
- [C4 Model](https://miro.com/app/board/uXjVMwF_wRc=/)
- [Kopia Warsztatów z DDD](https://miro.com/app/board/uXjVMvs5pZY=/)
- [Context Maps](https://miro.com/app/board/uXjVMv9tDJc=/)

## Repozytorium

- [Gitlab](https://gitlab.com/Linecode/workshops/technical-architecture-and-patterns/project)
- [Docs](https://linecode.gitlab.io/workshops/technical-architecture-and-patterns/project)