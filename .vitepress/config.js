import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Technical Architecture and Patterns",
  description: "",

  base: '/workshops/technical-architecture-and-patterns/docs/',
  outDir: './public',

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Przygotowanie Środowiska', link: '/' },
	  { text: 'Materiały', link: '/materialy'}, 
	  { text: 'Toolbox', link: '/toolbox/' },
	  { text: 'Bibliography', link: '/literature.html'}
    ],

    // sidebar: [
    //   {
    //     text: 'Examples',
    //     items: [
    //       { text: 'Markdown Examples', link: '/markdown-examples' },
    //       { text: 'Runtime API Examples', link: '/api-examples' }
    //     ]
    //   }
    // ],

    // socialLinks: [
    //   { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    // ]
  }
})
