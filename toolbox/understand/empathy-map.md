# Empathy Map

Brainstorm and record a particular stakeholder's responsibility, thoughts, and feeling to help the team develop a greater sense of empathy with stakeholders' goal.

## Benefits

- Discover your audience needs before developing an architecture description
- Help decide what information to include or exclude
- Create rubric for evaluating the effectiveness of an architecture description

## Participants

- Software Architect
- Development Team
- Small group of 3-5 or as a solo exercise

## Materials

- Choosen stakeholder, system or user
- Flipchart, markers, sticky notes or miro

## Steps

- Draw a grid on whiteboard or piece of paper. Label each quadrant -do, make, say, think
- Pick specific stakeholder, write his or her name in the middle
- Brainstorm tasks this person does, artifacts this person makes, things this person says, and feeling this person may have
- Write each idea on sticky note and place it to corresponding quadrant
- Review map and highlight insights

## Tips & Tricks

- Be Specific (Person over role)
- Validate findings
- Mention quality attributes, risks, other concerns
- Adapt fot understanding application end users, external systems

## Example

![](assets/empathy-map.png)
