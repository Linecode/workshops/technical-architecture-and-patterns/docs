# Back of the envelope estimation

The "back of the envelope" estimation approach refers to a rough calculation, often done quickly and informally, typically using simplified assumptions and rounded numbers. While it's not meant for precision, it's useful for getting a ballpark figure or checking the feasibility of more detailed calculations.

## Benefits

1. **Quick Decision Making**: Provides rapid insights to make initial decisions.
2. **Resource Efficient**: Doesn't require detailed data or sophisticated tools.
3. **Flexibility**: Can be done almost anywhere, even in casual settings.
4. **Confidence Check**: Acts as a sanity check for more detailed analyses.
5. **Identify Potential Challenges**: Helps in quickly spotting potential problems or hurdles in a project or idea.

## Participants

1. **Decision Makers**: They use the estimate to make informed initial decisions.
2. **Subject Matter Experts**: Individuals with knowledge or experience in the domain in question. They can provide insights or validate assumptions.
3. **Analysts or Strategists**: Those familiar with performing these types of quick calculations.

## Materials

1. **Paper & Pen**: True to its name, the back of an actual envelope can be used, or any scrap of paper will do.
2. **Calculator**: For slightly more complex calculations, though often the math is simple enough to do in one's head.
3. **Available Data or Research**: Any existing data, reports, or research that can inform the estimation.

## Steps

1. **Define the Problem or Question**: Clearly state what you're trying to estimate.
2. **Simplify the Problem**: Break down the problem into simpler, more manageable parts.
3. **Make Assumptions**: State any assumptions you're making. The key is to be reasonable, not precise.
4. **Perform Calculations**: Use basic math to estimate the answer based on your assumptions.
5. **Analyze the Result**: Reflect on the result. Does it make sense? Is it feasible?
6. **Adjust if Necessary**: Based on your analysis, adjust assumptions or calculations if needed and re-evaluate.

## Tips & Tricks

1. **Use Round Numbers**: They're easier to work with and reinforce that this is a ballpark estimate.
2. **Always Document Assumptions**: Makes the process transparent and allows others to understand or challenge the basis of the estimate.
3. **Use Familiar Benchmarks**: Drawing comparisons to known or familiar data points can help in making assumptions.
4. **Stay Conservative**: If uncertain, lean towards conservative estimates to avoid over-promising.
5. **Remember Its Limitations**: This approach is for initial estimates, not detailed planning. Always follow up with detailed analysis when necessary.

## Example

- **Problem**: Estimate the potential yearly revenue of a new coffee shop in a busy city district.
    - **Simplify**: Estimate daily customers, average spending, and yearly operating days.
    - **Assumptions**:
        - 300 customers daily (based on foot traffic observations).
        - $5 average spending per customer.
        - 300 operating days a year (factoring in holidays and potential closures).
    - **Calculation**: 300 customers x $5 x 300 days = $450,000 in potential yearly revenue.

The back of the envelope estimation provides a way to get quick insights without delving deep into detailed calculations, making it an invaluable tool for decision-makers across various domains.

### Numbers everyone should know:

#### Power of two

| Power | Approximate value | Full Name  | Short Name |
| ----- | ----------------- | ---------- | ---------- |
| 10    | 1 Thousand        | 1 Kilobyte | 1 KB       |
| 20    | 1 Milion          | 1 Megabyte | 1 MB       |
| 30    | 1 Bilion          | 1 Gigabyte | 1 GB       |
| 40    | 1 Trilion         | 1 Terabyte | 1 TB       |
| 50    | 1 Quadrillion     | 1 Petabyte | 1 PB       |

#### Latency

| Operation name                              | Time                   |
| ------------------------------------------- | ---------------------- |
| L1 cache reference                          | 0.5 ns                 |
| Branch mispredict                           | 5 ns                   |
| L2 cache reference                          | 7 ns                   |
| Mutext lock/unlock                          | 100 ns                 |
| Main memory reference                       | 100 ns                 |
| Compress 1K bytes with Zippy                | 10,000 ns = 10µs       |
| Send 2k bytes over 1 Gbps network           | 20,000 ns = 20µs       |
| Read 1 MB sequentially from memory          | 250,000 ns = 250µs     |
| Round trip within the same datacenter       | 500,000 ns = 500µs     |
| Disk seek                                   | 10,000,000 ns = 10ms   |
| Read 1 MB sequentially from the network     | 10,000,000 ns = 10ms   |
| Read 1 MB sequentially from disk            | 30,000,000 ns = 30ms   |
| Send packet CA(California)->Netherlands->CA | 150,000,000 ns = 150ms |

#### SLA

| 9    | 9    | .   | 9    | 9   | 9    | %     |
| ---- | ---- | --- | ---- | --- | ---- | ----- |
| 36d  | 3.5d |     | 9h   | 53m | 5m   | year  |
| 3d   | 7h   |     | 43m  | 4m  | 26s  | month |
| 2.5h | 14m  |     | 1.5m | 9s  | 0.8s | day   |
