# Response Measure Straw Man

The goal of a response measure straw man is to give stakeholders something to beat up until they arrive at their own answers. We do this by inventing a reasonable response measure for some quality attribute scenario as a way to kickstart discussions.

## Benefits

- Provides an example of a measurable response and response measure 
- Jump-starts thinking about quality attribute scenarios  
- Overcomes blank-page syndrome by providing something to edit instead of creating response measures from scratch

## Participants

Architects will often create straw man response measures on their own and validate with stakeholders later.

## Materials

- A list of raw quality attribute scenarios as described

## Steps

- For each quality attribute scenario, make up a response and response measure. The response should be a reasonable, best guess based on your knowledge and experience. 
  Response measures can be either outrageous or honest. 
  - Choose an honest response measure when you think you can confidently estimate a good measure. 
  - Choose an outrageous response measure when your confidence is low to help find the boundaries around acceptable behavior.
- Label the scenarios as having a straw man response measure to avoid potential future confusion. 
- Validate the scenarios and their response measures with stakeholders, such as during a [[Interview Stakeholders]] described, or [[Mini-Quality Attribute Workshop]] described.

## Tips & Tricks

- Use a straw man to understand the boundaries around acceptable behavior. 
- Responses should be correct for the scenario. The point is to zero in on an accurate and reasonable response measure. 
- Listen to your stakeholders once you get them talking. When presented with a wrong answer, many stakeholders will react with useful information. 
- Keep an eye out for anchoring. Anchoring is a cognitive bias where people let the first information they hear drive their decision making. The straw man should be a reasonable estimate or so outrageous it will be rejected outright. Exercise caution if your outrageous estimate is accepted.

## Example

| Quality Attribute | Response                                      | Straw Man Response Measure | Accepted Response Measure |
| ----------------- | --------------------------------------------- | -------------------------- | ------------------------- |
| Changeability     | Time required to add a new algorithm          | 6 months                   | 2 iterations              |
| Portability       | Effort required to move to new cloud provider | 3 person-months            | 4 person-days             |
| Performance       | Average response time under typical load      | 1 minute                   | 3 seconds max             |
| Scalability       | User load the system should be able to handle | 10 requests per second     | 140 requests per second   |
