The quality attribute web is a brainstorming and visualization activity to help elicit, categorize, refine, and prioritize stakeholder concerns and raw quality attribute scenarios. A quality attribute web captures stakeholders’ concerns. We write each concern on individual sticky notes. The web is drawn as a simple radar chart with relevant quality attributes written around the edge.

## Benefits

- Guide stakeholders to think about quality attributes instead of features. 
- Provide a visualization that shows how one system is different from another based on highly desirable properties. 
- Help stakeholders prioritize quality attribute scenarios before refining them.

## Participants

- Any stakeholders, including the team

## Materials

- If you are using a quality attribute taxonomy, prepare it ahead of time. You may find it helpful to print the web on poster paper instead of drawing it on a whiteboard. 
- Sticky notes, markers

## Steps

- Draw or post a blank quality attribute web so everyone can see it. The web can be created ahead of time if you know which quality attributes to include. If you’re not using a prepared web, brainstorm as a group to identify 5--7 quality attributes that are important to the stakeholders. 
- Brainstorm concerns and raw quality attribute scenarios as a group. Write each concern down on a sticky note and add it to the web near the quality attribute to which it most closely applies. When time expires, write down the concerns and use the information to create quality attribute scenarios.

## Tips & Tricks

- Some stakeholders will need help getting started. Be prepared to help them phrase their concerns initially. 
- Use dot voting to prioritize concerns on the web. 
- Don’t worry about getting perfect scenarios. A general thought, worry, response measure, or partial scenario is a great start
- Combine with the mini-quality attributes workshop, described, for a more comprehensive workshop.

## Example

![](assets/quality-attribute-web.png)