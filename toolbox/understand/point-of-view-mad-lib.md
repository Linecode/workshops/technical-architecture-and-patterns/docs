# Point-of-View Mad Lib

The Point-of-View (POV) mad lib summarizes business goals and other stakeholder needs in a memorable, engaging format. Here’s a basic POV mad lib template. You can use this template as is or create your own.

![](assets/point-of-view-scenario.png)

The format should be familiar to anyone who has written agile stories, though the emphasis is on stakeholders’ needs and how the overall system will provide value rather than specific features or functionality. You might think of it as a user odyssey, a statement that encompasses potentially multiple epics and stories.

## Benefits

- Develop empathy for stakeholders’ needs. 
- Articulate business goals in a user-focused way. 
- Use to start the conversation about business goals.

## Participants

Any stakeholders. This activity can be done alone or as a small group of 2--3 people. If necessary, a larger group can be divided into smaller subgroups of 2--3 people each.

## Materials

- Before the activity, identify the list of stakeholders for which you’ll produce mad libs. This list can be created just-in-time with participants before introducing the mad lib activity. 
- Markers and sticky notes for each group. Enough paper for each group to produce one mad lib per stakeholder.

## Steps

- Introduce the activity by sharing the goal of the exercise. 
- Describe the mad lib template and do a warm-up exercise to ensure participants understand the mad lib format. Everyone should participate in the warm-up. 
- Introduce the first stakeholder. Briefly share any information known about the stakeholder and discuss their needs as a group.
- Give each group 90 seconds to create a mad lib. 
- Repeat steps 4--5 until all stakeholders have been covered. 
- Share the mad libs produced and briefly discuss as a group. Consensus is not required as a part of this activity.

## Tips & Tricks

- Be specific. Pick an actual person if you can. 
- Don’t worry about phrasing at first. It can be difficult to find exactly the right words. Getting the ideas out is more important. The impact of each mad lib should be outcome focused. 
- Try the 5 Whys technique to help get to the bottom of stakeholders’ real needs.

## Example

> Mayor van Damme wants to reduce procurement costs by 30 percent because he wants to avoid cutting funding to education in an election year. 

> Mayor van Damme wants to improve city engagement with local businesses because it may improve the local economy when local businesses win contracts. 

> The Office of Management and Business wants to cut the time required to publish a new RFP in half because it improves services and reduces costs at the same time.


## Alternatives

### Who? What? Why? (Design Hills)

**Who?** A specific stakeholder who is affected by the software to be built. 
**What?** Something the stakeholder will be able to accomplish with the software that he or she could not do before.
**Wow!** A significant, measurable outcome that directly results from having used the software to complete the task.

![](assets/point-of-view-full.png)

### Subject Outcome Context

**Subject** A specific person or role. 
**Outcome** A specific and measurable description of how the world changes if the system is successful. 
**Context** Describes the conditions around the goal so the team can develop empathy and a deeper understanding of the need.

| Stakeholder     | Goal                            | Context                                                                    |
| --------------- | ------------------------------- | -------------------------------------------------------------------------- |
| Mayor van Damme | Reduce procurement costs by 30% | Strong desire to avoid making budget cuts to education in an election year | 
