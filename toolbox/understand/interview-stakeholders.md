# Interview Stakeholders

Just talk

## Benefits

- Focus on general information gathering
- Format allows for open back-and-forth discussion
- Provides background information that can be used to prepate for other workshops or activities
- Quickly validate quality attribute scenarios and other ASRs
- Creates a direct connection between stakeholder and architects

## Participants

- one-on-one or in small stakeholder groups.
- Architect leads the interview
- Stakeholders are the interview subjects
- Additional team member may observe an interview

## Materials

- Interview goal and questions
- Pen and paper or laptop
- Voice recorder could be beneficial, just to help focus on conversation

## Steps

- Explain Goal
- Ask the subject from your planned interview checklist
- Follow up with clarifying questions as needed to be sure
- Conclude the interview by thanking the subject
- Directly after the meeting hot down your general impressions including any themes, technical asides and design thoughts
- Once all interviews have been completed analyze the data collected.
- Onec all interviews have been completed hold a debrief meeting with the team and stakeholders to share insights

## Tips & Tricks

- Avoid interviewing stakeholders about architectural concers too early.
- Phrase questions so the subject can share their true thoughts. Avoid leading the subject
- When possible use the subject's words when summarizing ideas.
- Talk to real users and primary stakeholders
- Use data to help jump start conversations
- Record session

## Example