# Impact Mapping

Impact mapping is a strategic planning technique that visually displays the relationship between goals, actors, impacts, and deliverables. It's a collaborative approach designed to ensure that projects and products deliver the most valuable features.

## Benefits

1. **Goal-Oriented**: Keeps the team focused on high-level objectives, ensuring that every task aligns with the desired outcome.
2. **Visual Representation**: Provides a clear visualization of how tasks and deliverables relate to the larger goal, aiding understanding and communication.
3. **Collaborative**: Engages stakeholders, designers, and developers from the onset, ensuring a shared understanding and buy-in.
4. **Adaptive Planning**: Makes it easier to adapt to changes, pivots, or new priorities as they arise.
5. **Efficient Use of Resources**: Helps prioritize features or tasks that provide the most value.

## Participants

1. **Product Owners/Managers**: They set the goals and ensure alignment with business objectives.
2. **Stakeholders**: Individuals or groups who have an interest in the outcome.
3. **Developers & Designers**: They provide insights into what's feasible and can suggest alternative solutions.
4. **End-users or User Representatives**: Their feedback ensures that the solutions align with user needs.

## Materials

1. **Large Whiteboard or Flip Chart**: This will be the main canvas for mapping.
2. **Sticky Notes**: Different colors can represent goals, actors, impacts, and deliverables.
3. **Markers**: For writing on the sticky notes and drawing connections.
4. **Digital Tools**: For remote teams, tools like Miro or Mural can be used for collaborative mapping.

## Steps

1. **Define the Goal**: Start by identifying the primary business objective or goal.
2. **Identify Actors**: List down individuals or groups that can influence the outcome.
3. **Determine Impacts**: For each actor, identify behaviors or actions they could take that would contribute to achieving the goal.
4. **List Deliverables**: For each impact, brainstorm features or tasks that can stimulate or support the desired behavior.
5. **Draw Connections**: Use lines to connect the goal to actors, actors to impacts, and impacts to deliverables, creating a visual map.
6. **Review and Adjust**: Continuously revisit the map to refine and adjust based on feedback and changing priorities.

## Tips & Tricks

1. **Keep It Simple**: Start with high-level ideas and break them down. Avoid getting too detailed initially.
2. **Engage Everyone**: Ensure active participation from all team members. The diversity of perspectives enhances the quality of the map.
3. **Use Icons or Symbols**: They can help to quickly identify types of tasks, risks, or dependencies.
4. **Regularly Revisit**: As with any planning tool, the impact map should be dynamic. Review and adjust it regularly.

## Example

- **Goal**: Increase e-commerce website sales by 20% in the next quarter.
    - **Actor**: Existing customers.
        - **Impact**: Make more frequent purchases.
            - **Deliverable**: Implement a loyalty rewards program.
    - **Actor**: New visitors.
        - **Impact**: Convert to paying customers.
            - **Deliverable**: Improve website user experience and introduce welcome discounts.

By following the steps and tips mentioned above, teams can make effective use of the impact mapping technique to ensure their projects align with overarching goals and deliver value.