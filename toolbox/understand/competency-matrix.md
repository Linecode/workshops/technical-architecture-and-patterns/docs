# Competency Matrix

A Competency matrix is a management tool designed for small and large organizations for measuring and understanding the skills within a workforce. It facilitates practical skills management practice and ultimately returning benefits to the organization. In addition, it improves the visibility, knowledge, and understanding of its ability to deliver quality results as expected. It is a foundational component for managing skills.

## Benefits

- Identifying the proper task force
- Trace employee development
- Identify missing competencies
- Identify competency-employee mismatch
- Predict potential competency loss threat

## Participants

- Development Team

## Materials

- Miro

## Steps

- Explain goal
- Introduce Legend
- Prompt people for raw ideas what skills they are good at (10 - 15 minutes)
- Wrap up 

## Tips & Tricks

- Icebreaker: Add one skill which you are not good at: e.g. training Large Language Models

## Example



#### Competency Matric

| Legend | Level        |
| ------ | ------------ |
| G      | Expert       |
| Y      | Practitioner |
| R      | Novice       |

|                  |            | John | Emma | Liz |
| ---------------- | ---------- | ---- | ---- | --- |
| Cocktails        | G: 1, Y: 1 | R    | G    | Y   |
| Customer Service | Y: 2       | Y    | Y    | R   |


#### Skills Matrix

| Proficiency Level |                                      |
| ----------------- | ------------------------------------ |
| 0                 | No Experience or knowledge           |
| 1                 | Limited Experience or knowledge      |
| 2                 | Reasonable experience or knowledge   |
| 3                 | Considerable experience or knowledge |
| 4                 | Expert experience or knowledge       |

| Interest Level |                                             |
| -------------- | ------------------------------------------- |
| 0              | Has no interest in applying this capability |
| 1              | Is interested in applying this capability   |


| Capabilities       | Jonathan  | Aiden     | Tracy     |
| ------------------ | --------- | --------- | --------- |
| Data Analysis      | P: 3 I: 1 | P: 2 I: 1 | P: 0 I: 0 |
| Data Visualization | P: 0 I: 0 | P: 0 I: 0 | P: 3 I: 1 |