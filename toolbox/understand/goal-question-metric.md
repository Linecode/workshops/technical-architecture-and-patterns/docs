# Goal-Question-Metric

Approach for identifying metrics and response meassures. 

## Benefits

- Emphasizes using stakeholder goals as the basis for measures
- Shows clear lineage from data to stakeholder goals by way of the questions that must be answered to decide whether a goal is satisfied
- Flexible approach that can help teams think about metrics in a variety of situations

## Participants


- Solo or in small group 2-5 people. Anu mix of stakeholders will work

## Materials

- Whiteboard or flipchart papers, markers
- Goals to be explored can be optionally identified before workshop

## Steps

- Write goal at the far left of the whiteboard
- Prompt participants to provide questions (What questions would you need to answer to know if we've met this goal?)
- Write each question to the right of the goal. Draw lines from the goal to the question.
- Explore each question to identify metrics needed to answer the question.
- Write each metric to the right of the questions. Draw lines from each question.
- Repeat for any related reasonable goal
- Identify data required to compute each metric.
- Write the data needs to the right of metrics. Draw lines from metric to data need.
- For each piecie of data identified, determine where you can get the data. 
- Write down each data source for each bit of data. Describe the cost of gathering data from ech of the data sources
- Prioritize data and metrics. Cleary identify must have metrics
- Record the results

## Tips & Tricks

- Plenty of space for drawing
- Look opportunities for reuse. Metrics can be used to answer more than one question.
- Data source and data are likely to influence the architecture, but look for opportunities to gather data outside of the architecture
- Record results in a table to validate with stakeholders later.

## Example

![](assets/gqm.png)