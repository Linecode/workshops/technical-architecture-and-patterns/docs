# Mini-Quality Attribute Workshop

The mini-Quality Attribute Workshop (mini-QAW) is a lean, facilitated workshop designed to help you talk about quality attributes with stakeholders early in a system’s life. During a mini-QAW, you’ll collaborate as a group to quickly identify, develop, and clarify quality attributes with the help of a quality attribute taxonomy. By the end of the mini-QAW, you’ll have a prioritized list of quality attribute scenarios and a wealth of contextual information about the system to be designed.

## Benefits

- Walk through the essential steps of a traditional quality attribute workshop in only a few hours.
- Quickly identify raw quality attributes and prioritize them before refining into full scenarios. 
- Provide opportunities for stakeholders to riff on each other’s ideas. 
- Create a forum for open discussion among stakeholders to discuss quality attribute concerns, risks, and other general concerns about the software system.

## Participants

- A facilitator, usually the software architect. A small group of stakeholder participants. 
- This workshop works best in small groups of 3--5, with a maximum size of about 10 participants.

Host multiple workshops if necessary to keep the group size down. When hosting multiple workshops, review scenarios with all groups once the workshops have concluded.

## Materials

- Before the workshop, prepare a quality attribute taxonomy. The quality attribute taxonomy is a set of predefined quality attributes highly relevant to the type of system you are building. An example of a quality attribute taxonomy for service-oriented architectures is available from the Software Engineering Institute. The taxonomy will be used to facilitate structured brainstorming. 
- Prepare graphical quality attribute scenario templates in the style of the examples. Use these templates to capture scenarios during the workshop.
- If desired, prepare a quality attribute web, shown, on poster-sized paper for use during the workshop. If not using a pre-printed taxonomy web, draw a web at the start of the workshop. 
- Sticky notes and markers for participants

## Steps

- Present the workshop goals and agenda. 
- Teach participants what they need to know about quality attributes. Describe the quality attribute taxonomy you’ll use during the workshop. 
- Display or draw the quality attribute web so everyone can see it. 
- Brainstorm raw quality attribute scenarios using either structured brainstorming or a questionnaire. Instruct participants to write one idea per sticky note and place them directly on the displayed taxonomy web. Read the posted raw scenarios out loud as they are placed on the web. If this prompts participants to think of new scenarios, record and post them on the web too.
- After the brainstorming phase, prioritize the quality attributes and raw scenarios using dot voting. Participants get 1/3 the number of identified raw scenarios. For example, if there are 25 sticky notes on the web, everyone gets 8 votes to spend however they please. Participants also get 2 votes for overall quality attributes. Everyone votes at the same time. 
- Refine the top raw scenarios as a group until time runs out using the six-part scenario template shown. Remaining work must be done as homework.
- As homework, refine the top raw quality attribute scenarios. Present the top refined quality attribute scenarios in a follow-up meeting to verify the scenarios and relative priority.

## Tips & Tricks

- Keep your taxonomy small, 5--7 quality attributes max. 
- Use the web visualization to drive the workshop. Put the sticky notes close to related quality attributes. 
- Don’t worry about creating formal scenarios during brainstorming. 
- Ask probing questions about the stimulus, response, environment.
- Pay attention when stakeholders sound worried about something. Stakeholders’ worries are often the source of a possible scenario. 
- Watch out for features and functional requirements. 
- Do not skip the homework. This is the most important part! 
- If workshop participants are not co-located, select screen-sharing software all participants can use or consider using a digital whiteboard application such as Mural. See ​Work with Remote Teams​ for more remote facilitation tips.

## Example

| Agenda Item                                | Timing               | Hints                           |
| ------------------------------------------ | -------------------- | ------------------------------- |
| Introduce the Mini-QAW                     | 10 minutes           |                                 |
| Teach participant about Quality Attributes | 15 minutes           | Set participants up for success |
| Brainstorm RAw Scenarios                   | 30 minutes - 2 hours | Walk the System Properties Web  |
| Prioritize raw scenarios                   | Until time runs out  | Finish as homework              |
| Review the results                         | 1 hour               | Separate, future meeting        |

