# Choose One Thing

Discuss priorities with stakeholders by presenting them with extreme choice: "If you only get one thing what will it be?".

## Benefits

- Clearly communicates priorities
- Force conversation about why and what
- Makes it obvious when stakeholders disagree

## Participants

All Stakeholders

## Materials

- List of alternatives, such as quality attributes, or other difficult trade-offs, such as cost, schedule, and features

## Steps

- Explain rules:
	- Stakeholder can choose only one option
	- Remind participant that this doesn't mean that only this one think will be build
- Present a set of options
- Discuss options
- Force stakeholders to pick one
- Discuss why this option was selected
- Pick another topic

## Tips & Tricks

- Better to play it at the start, this i s easier at hypothetical level than real
- Quality attribtues that are in tension with one anotjer should be pitted against one another
- Use it to prioritize influential functional requirements
- Keep it informal

## Example

| Matchup                                | Stakeholder's Choice                                                                                                    |
| -------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Faster performance or greater accuracy | Faster performance, assuming accuracy met a required minimum threshold                                                  |
| Cost vs time-to-market                 | Time-to-market, stakeholders were willing to accept greater technical debt to get required features by a specific date. |
| Usability vs security                  | x                                                                                                                       |
| Availability vs cost                   | x                                                                                                                       |

## Alternative

Slider comparisons: 

![toolkit-skiders-comparisons](assets/toolkit-skiders-comparisons.png)