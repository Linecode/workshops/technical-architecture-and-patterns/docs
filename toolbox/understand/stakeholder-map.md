# Stakeholder Map

A stakeholder map is a network diagram of the people involved with or impacted by the software system. Use this method to visualize the relationships, hierarchies, and interactions between all the people who have an interest in the software system to be built.

## Benefits

- Identify more stakeholders than just the usual suspects. 
- Determine who to talk to about requirements. 
- Help the team empathize with people and not just focus on technology. 
- Create a snapshot of the system context and who’s involved. 
- Use as a document to bring new teammates up to speed or to assist with architecture validation.

## Participants

Whole team, known stakeholders This activity can be conducted alone or with groups of 25 or more people depending how much physical space is available.

## Materials

- Miro: https://miro.com/miroverse/stakeholder-map/

## Steps

- Introduce the activity by sharing the goal of the exercise. You might start by saying, For the next 30 minutes we’re going to explore who our stakeholders are. Once we have a better idea of who has a stake, we’ll come up with a plan for who we’re talking to first. 
- Share the guidelines and hints for creating a stakeholder map. 
- Start the activity. Working together, everyone adds and annotates stakeholders collaboratively until time runs out or the map seems complete. 
- Once the map is complete, ask participants to share observations about the map. Are there interesting connections or unexpected stakeholders? Who are the most important stakeholders? 
- Take a picture of the map and store it in your team’s wiki.

## Tips & Tricks

- Use simple icons to represent individual people; use multiple icons to represent groups. 
- Be specific when naming stakeholders. Think about their roles or in some cases specific names. 
- Use speech bubbles to represent stakeholders’ needs or thoughts. 
- Connect people using arrows to show relationships and influence. Label connections to describe relationships.
- Encourage participants to look beyond the obvious stakeholders if they stall out during the activity. 
- Nudge wallflowers, participants who are just watching, to pick up a marker and add to the map.

## Example