# List Assumptions

## Benefits

- Head off misunderstandings about the true goals and requirements
- Great for ad hoc analysis. Does not require a formal workshop or agenda
- Avoid missing important requirements

## Participants

- Whole team working in pairs or small groups (3-5)
- Could be done solo but than need to share with someone: otherwise assumptions will remain hidden

## Materials

- Any writting surface

## Steps

- Kick off the activity with the adage: You know what happens when we assume to much, right? It makes an ass out of you and me!
- Explain the goal: Over the next 15 minutes we-re going to write down all the assumptions we have about the system
- Prompt participant by focusing on an area where assumptions need to be flushed into the open
- Write down the assumptions so everyone can see them
- When pace slows, move on to the next topic or end session by planning follow-up actions

## Tips & Tricks

- Start by asking: What do we think we know about X?
- Write down everything mentioned, even if it seems like common knowledge
- Pause the discuss assumptions that are surprising or generate a reaction from one or more participants
- Record the assumption in your team wiki

## Example