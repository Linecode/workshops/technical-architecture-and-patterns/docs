# Architecture Briefing

This brief (no pun intended) presentation is used to bring stakeholders up to speed about some part of the architecture. By the end of the briefing, participants are prepared to provide meaningful feedback about the architecture. 

Architecture briefings are a common practice used by traditional building architects to educate clients and share progress. The same general practice has been used in software development for decades. The idea of using an architecture briefing with software has been proposed by many, including Stuart Halloway and Patrick Kua.

## Benefits

- Quickly bring stakeholders up to speed so they can ask questions and point out issues in a design.
- Foster a sense of shared ownership over the architecture. 
- Enable more stakeholders to provide feedback, ensuring a diversity of perspectives critically evaluate the design. 
- Promote accountability in architectural decision making. 
- Create a platform for teaching and learning architecture design. Teammates will be exposed to other people’s approaches to architecture design and have a chance to practice articulating designs concisely.

## Participants

The architect presents the briefing. 

Stakeholders and knowledgeable nonstakeholders attend the briefing. The briefing should be open to a wide audience, including people with no prior knowledge of the architecture under discussion.

## Materials

Briefing presentation. This will usually be slides but can also be a whiteboard talk. As a rule of thumb, preparing for the briefing should require no more than about twice the briefing length. For example, preparing for a 30-minute presentation should take about an hour. 

Audience members should bring supplies to take notes.

## Steps

1. Welcome the audience, introduce the architect, and share the ground rules for the briefing. 
   > Here are some sample ground rules you can use: Audience’s job: Question everything. Please hold questions and comments until the end. Pay attention. Take notes. Think about: What is missing? How does this compare to your experience? Do you agree with the decisions? Do you understand why a decision was made? Be respectful; remember, your briefing is next!
   
2. The architect presents the briefing. The presentation ends when time expires or the architect is done, whichever comes first. 
3. Open the floor for comments and questions. Team members may optionally join the architect to field questions. 
4. Conclude the briefing by thanking the audience and architect. The briefing ends when there are no more questions or the time has expired.

## Tips & Tricks

- Host briefings at the same time and place at a regular interval. 
- Publish slides after the briefing and record it if possible.
- The team presenting the briefing should appoint a note taker during the questions portion of the briefing. 
- Audience questions should be tough, but constructive.

## Example

Here is one possible outline for architecture briefings. The specific outline may vary depending on the type of systems you build.

> Elevator pitch: what overall business problem are you solving? 
> Overview and context 
> Top quality attributes 
> Relevant views 
> Key design decisions with rationale 
> Alternatives considered
> Current status: quality, work remaining, next steps 
> Costs 
> Top risks and other concerns 
> Future plans


[Example](https://github.com/stuarthalloway/presentations/wiki/Architectural-Briefings)