# Risk Storming

A collaborative, visual technique for identifying risks in the architecture. Risk storming was proposed by Simon Brown in Software Architecture for Developers.

## Benefits

- Quickly identify risks in the proposed system architecture.
- Visualize the system by considering the level of risk. 
- Constrain risk identification to architectural concerns. 
- Provide a platform for all team members to elevate their concerns.

## Participants

Small groups of 3--7 developers. Participants must be familiar with the architecture. This workshop can be self-facilitated by experienced participants.

## Materials

- Views of the architecture. These can be created just-in-time or printed from appropriate sources.
- Sticky notes (three different colors), markers 
- Large paper, flipcharts, or whiteboard with appropriate markers

## Steps

1. Set the expectations for the workshop by explaining the workshop goals. For example, By the end of this workshop we will have a list of prioritized risks to help us decide on next steps. 
2. Sketch relevant views. All participants should help sketch views of the architecture on whiteboards or paper. Include a range of views. 
3. Brainstorm risks. Working individually, participants will write one risk per sticky note. The color of the sticky note used to capture the risk should correspond to the risk’s degree of exposure: high, medium, or low. Exposure is a relative qualification of how bad a risk is by considering probability, impact, and time frame. 
4. Cluster risks on the diagrams. Participants place their risks on the diagrams where they think the risk most directly applies.
5. Prioritize and discuss the identified risks. Look at clusters of sticky notes, high-exposure risks, or other interesting patterns. 
6. Develop mitigation strategies and decide on next steps.

## Tips & Tricks

- Place sticky notes directly on the diagrams. 
- Stick duplicate risks directly on top of each other.
- Leave time for team discussions. This is the most important part. 
- Use no more than 2--3 sketches. More than that can be overwhelming.

## Example

![](assets/risk-storming.png)