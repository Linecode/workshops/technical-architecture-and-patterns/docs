# Question-Comment-Concer

This is a collaborative, visual activity that gets the whole team talking about the architecture—what they know, what they don’t know, and what keeps them up at night. Use this activity to shine a light on knowledge gaps, articulate issues, and establish known facts about the architecture. 

The inclusion of comments during the workshop is meant to fast-track issue resolution. Issues that result from simple gaps in understanding can be resolved immediately so the team can focus on the bigger concerns. As an architect you can even choose to add questions during the workshop as a simple sanity check (described).

## Benefits

- Promote knowledge sharing by flushing questions into the open along with potential answers. 
- Visualize high-risk, mysterious, or troublesome parts of the system. 
- Identify areas in need of further research and exploration. 
- Foster shared ownership over the architecture and the direction it takes.

## Participants

Whole team, about 3--7 participants

## Materials

- Views of the architecture. These can be created just-in-time or printed from appropriate sources. 
- Sticky notes (three different colors), markers 
- Large paper, flipcharts, or whiteboard with appropriate markers

## Steps

1. Start the workshop by explaining the goals. For example, In this workshop we will learn what we know, what we think we know, and what worries us about the architecture.
2. Sketch relevant views. All participants should help sketch views of the architecture on whiteboards or paper.
3. Brainstorm questions, comments, and concerns. Working together, participants will write one item per sticky note and immediately place them on the relevant view. 
4. Stop the exercise when time expires or as the rate of stickies slows. 
5. Observe and reflect. What does the team notice about the diagram? What is interesting about where sticky notes landed? Are there areas of particularly high concern or uncertainty? Were there any areas with many questions that were quickly answered? 
6. Extract themes. Read through the sticky notes and write down common themes that emerge. 
7. Decide on next steps. Briefly brainstorm actions that should be taken next. Prioritize and assign responsibility for next steps.

## Tips & Tricks

- Assign a color to each type of item before brainstorming starts. Create a legend that is visible to all participants. 
- Comments can be facts, new ideas, tidbits of knowledge, or answers to questions raised during the workshop. To answer a question, simply write the answer on a comment sticky note and place it directly on top of the question it answers. 
- Concerns can be known problems, risks, or general worries. 
- Pay attention to questions. Questions can expose gaps in understanding, mismatches in expectations, or areas in need of further exploration.

## Example

![](assets/qcc.png)