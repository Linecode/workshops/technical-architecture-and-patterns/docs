# One Door vs Two Doors Decisions

The "One Door vs Two Doors Decisions" concept, often associated with Jeff Bezos of Amazon, represents a decision-making approach based on the reversibility of decisions. A "One Door" decision is one that, once made, you pass through and can't easily return. In contrast, a "Two Doors" decision is reversible, like walking through a door and being able to return if needed.

## Benefits

1. **Risk Management**: Distinguishes between decisions that need thorough deliberation and those that can be acted on quickly.
2. **Faster Decision Making**: Encourages swift action on reversible decisions, fostering a culture of experimentation.
3. **Resource Optimization**: Allocates resources and time more efficiently based on decision type.
4. **Empowers Teams**: Gives teams the confidence to make decisions knowing they can backtrack if necessary.
5. **Facilitates Innovation**: The possibility of reversal promotes taking risks and trying new things.

## Participants

1. **Leaders & Decision Makers**: Set the tone for the organization and model the approach.
2. **Teams**: Use the framework to assess and make day-to-day decisions.
3. **Project Managers**: Guide teams in project-related decisions based on the reversibility principle.
4. **Risk Management Professionals**: Help in evaluating the potential implications of decisions.

## Materials

1. **Whiteboard or Large Paper**: For mapping out decisions and categorizing them.
2. **Markers**: For drawing and writing.
3. **Sticky Notes**: For placing different decisions on a decision map.
4. **Digital Tools**: Platforms like Miro or Mural can be useful for collaborative decision discussions.

## Steps

1. **List Down Decisions**: Enumerate upcoming or pending decisions.
2. **Assess Reversibility**: For each decision, determine if it's a One Door (irreversible) or Two Doors (reversible) decision.
3. **Allocate Resources**: One Door decisions might require more resources, time, and analysis due to their nature.
4. **Act on Two Doors Decisions**: Encourage swift action on reversible decisions and promote a learn-and-adapt mindset.
5. **Review Decisions**: Especially for Two Doors decisions, set checkpoints to review and determine if a reversal or pivot is needed.
6. **Document Lessons Learned**: Regardless of outcome, document learnings for future reference.

## Tips & Tricks

1. **Encourage a Growth Mindset**: Mistakes are okay, especially with Two Doors decisions. Focus on learning.
2. **Clarify Ambiguities**: If there's uncertainty about the nature of a decision, seek input.
3. **Communicate**: Ensure that the team understands the rationale behind decisions, especially for One Door choices.
4. **Flexibility**: Remember that some decisions may seem reversible but can evolve into irreversible ones over time, and vice versa.
5. **Regularly Revisit**: Continually reassess decisions, especially if new information becomes available.

## Example

- A company is deciding on rolling out a new feature for its product.
    - If the feature, once launched, will significantly alter the user experience and can't be easily rolled back without affecting customer trust, it's a **One Door decision**.
    - If the feature can be introduced as a beta, tested with a subset of users, and easily rolled back if it doesn't work, it's a **Two Doors decision**.

By employing the "One Door vs Two Doors Decisions" approach, organizations can cultivate a more agile, experimental, and risk-aware culture, leading to more informed decisions and better outcomes.