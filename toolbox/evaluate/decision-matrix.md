# Decision Matrix

A decision matrix is a visual comparison of how various alternatives stack up against one another. Use a decision matrix to qualify design alternatives so a decision can be made. A decision matrix can also be used in documentation as a part of the design’s rationale.

## Benefits

- Use to compare a variety of decisions such as patterns, technologies, or frameworks. 
- Visualize relative strengths and weaknesses among decisions. 
- Focus attention on essential factors when comparing and contrasting alternatives. 
- Facilitate open discussion about trade-offs among alternatives.

## Participants

The architect is responsible for ensuring the matrix is filled in accurately. Stakeholders validate the evaluation factors.

## Materials

- Identify a list of architecturally significant requirements, especially quality attribute scenarios to be used as the properties for comparison. 
- Before starting the analysis, identify at least two design alternatives for comparison.

## Steps

1. Identify evaluation factors. Collaborate with stakeholders to agree on the factors used to compare and contrast alternatives. 
2. Establish a rubric. Collaborate with stakeholders to decide how design alternatives will be scored. For guidance on defining a rubric, see ​Define a Design Rubric​. 
3. Do the analysis and fill in the matrix. Share the matrix with stakeholders. 
4. Verify the analysis and discuss your recommended decision.

## Tips & Tricks

- Use qualitative comparisons unless you performed quantitative analysis. For example, performance or availability can only be quantified if you ran tests. 
- Consider no more than seven factors in the same matrix. 
- Compare up to five design options in the same matrix. Use multiple matrices with a larger number of options. Take good notes when filling in the matrix. 
- The analysis is as important as the results and can provide design rationale for decisions.

## Example

|                   |                                                                                              |
| ----------------- | -------------------------------------------------------------------------------------------- |
| Strongly Promotes | The design option actively helps you to achieve the system property.                         |
| Promotes          | The design options allow you to achieve the system property.                                 |
| Neutral           | The design option neither helps nor hurts the system property                                |
| Inhibits          | The design options make achieving the system property slightly more difficult               |
| Strongly Inhibits | The design options make it costly or significantly difficult to achieve the system property |

Legend: 

|                   |     |
| ----------------- | --- |
| Strongly Promotes | ++  |
| Promotes          | +   |
| Neutral           | o   |
| Inhibits          | -   |
| Strongly Inhibits | --  | 

Decision Martix:

|                                     | 3-Tier | Publish - Subscribe | SOA |
| ----------------------------------- | ------ | ------------------- | --- |
| Availability(Database unavailable)  | +      | o                   | +   |
| Availability(Uptime req)            | o      | o                   | o   |
| Performance (5s respond time)       | o      | -                   | +   |
| Security                            | o      | -                   | o   |
| Scalability                         | o      | o                   | +   |
| Maintainability (Team knowledge)    | +      | -                   | o   |
| Buildability (Implementation risks) | ++     | -                   | --  |
