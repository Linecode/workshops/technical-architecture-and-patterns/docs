# Code Review

A form of peer review in which code is incrementally inspected with an eye toward architectural concerns as the code is developed. Code review is a fantastic practice that every team should do anyway. Extending code reviews to include architectural concerns makes them even more powerful.
Incrementally inspecting code as the architecture manifests helps fight architectural rot by keeping tabs on the system’s evolution relative to the planned design. Reviews are also a great time to identify design inflection points that emerge during development. Such inflection points may require further analysis. 

Every review presents a potential coaching opportunity. Watch out for synchronization mismatches in mental models so you can fix them before real problems arise. Coach teammates on architectural principles as well as the specific architecture you’re developing together.

## Benefits

- Keep architecture design at the forefront of every developer’s mind. 
- Allow finer-grained details to emerge without losing a connection with coarser-grained architectural concerns. 
- Manage emergent details that can cause problems in the architecture. 
- Influence the detailed design as required. 
- Create teachable moments to grow the team’s architecture design competence.

## Participants

## Materials

## Steps

## Tips & Tricks

## Example