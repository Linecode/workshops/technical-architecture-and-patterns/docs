# Sanity Check

A fast, simple exercise designed to expose issues in team communication or understanding. Sanity checks verify everyone is indeed on the same page. They can also identify opportunities for improving team operations, artifacts, and design methods. 

When designing a sanity check, think back to the pop quizzes you dreaded in elementary school. Any short-burst activity that forces teammates to actively think about the architecture is a great sanity check candidate. Verbal sanity checks are an efficient way to end most collaborative design sessions.

## Benefits

- Reinforce architectural responsibility across the team.
- Identify problems caused by misunderstandings and gaps in knowledge early. 
- Create teaching and coaching opportunities. 
- Document knowledge the team feels is essential to the design. 
- Pinpoint opportunities for improving artifacts and communication. 
- Uncover the unknown unknowns.

## Participants

Whole team, assign one team member to prepare the sanity check.

## Materials

A prepared exercise or quiz.

## Steps

1. Remind the team that sanity checks are for process improvement and helping uncover knowledge gaps before they become problems.
2. Complete the exercise. 
3. Review answers to the exercise. Briefly discuss answers that differed from the key.
4. Decide whether follow-up actions are required. If so, the person who led the sanity check will lead the team in determining next steps.

## Tips & Tricks

- Keep it simple and short. A good sanity check can be completed in 5 minutes or less. 
- Always remember: Sanity checks are really about improving the team’s knowledge. Do not use sanity checks to punish or promote teammates. 
- Share responsibility for creating sanity checks among the team. 
- Host regular sanity checks—for example, at the beginning or end of a weekly status meeting or retrospective. 
- Get creative and use a variety of formats. This keeps the sanity checks fresh and helps expose different kinds of understanding gaps.

## Example

![](assets/sanity.png)