# Sketch and Compare

Sometimes a design only seems good because there isn’t a baseline for comparison. With the sketch and compare activity, we create two or more alternatives of the same design so it’s easier to see the pros and cons. 

Any design alternatives can be sketched and compared. This includes current and future, ideal and reality, technology A and technology B, and many others. Sketching the extremes can also be compared. To do this, pick a quality attribute or design concept and design the architecture for that one thing at the exclusion of all else. Then pick another high-priority quality attribute or interesting design concept and sketch an alternative design for comparison.

## Benefits

- Expose both the positive and negative aspects of a decision by comparing it to something else. 
- Create a platform for discussion and build consensus around a design decision. 
- Avoid buyer’s remorse when making a design decision by doing at least a basic comparison.

## Participants

This activity works best in small groups of 3--5 people, including the architect and other stakeholders.

## Materials

- Whiteboard or flipchart, markers 
- Alternatively, prepared views (for example, in slide software), projector

## Steps

1. Establish the goals of the activity by saying something like, It seems like there are at least two alternative designs on the table. Let’s put them side by side and pick one. 
2. Sketch or show the alternative designs so everyone can see them. 
3. Open the discussion by pointing out an advantage or disadvantage of one design compared to the other. Invite others to share their thoughts.
4. Write down participants’ ideas as they share them. If necessary, change or annotate the diagrams to clarify meaning or add new insights. 
5. As the group begins to reach consensus, summarize the decisions made. Give a just-in-time sanity check (introduced) to verify that participants understand and agree with the decisions. 
6. Take pictures, record the decisions in your team wiki, and use the discussion to help flesh out design rationale.

## Tips & Tricks

- Help participants avoid argumentative confrontations and encourage constructive participation. Sometimes participants will pick sides and entrench themselves, strongly favoring one design over the other.
- Be ready to sketch compromises as the discussion progresses. Some of these new ideas will become new design alternatives. 
- Always summarize the findings. Skipping the final summary can leave participants confused about the decisions made. 
- Follow up with skeptics to win them over and drive consensus.

## Example

![](assets/sketch.png)

