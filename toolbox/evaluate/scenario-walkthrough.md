# Scenario Walkthrough

Describe step-by-step how the architecture addresses a specific quality attribute scenario. Scenario walkthroughs can be used any time but are most applicable early in the life of the software system, before the system’s behavior can be observed directly. 

A scenario walkthrough is like telling a story about the architecture. Pick a quality attribute scenario and describe what the system would do in response to the scenario stimulus. As you walk through the various elements in your design, show how the quality

## Benefits

- Assess the architecture design early, even while it’s only on paper.
- Identify different concerns in the architecture.
- Reason about how the architecture will respond to different stimuli. 
- Qualify the design. Walkthroughs are not strict pass or fail. 
- Quickly determine the extent to which the architecture promotes or inhibits different quality attributes.

## Participants

Scenarios walkthroughs require that the following roles are filled:

- The architect is someone knowledgeable in the architecture’s design. This person describes how the system responds to stimuli. 
- The recorder take notes during the meeting. This person will write down any issues, risks, unknowns, gaps, and other general concerns raised during the session. 
- The reader reads scenarios to start the discussion and facilitates the walkthrough. This person is also the session moderator. 
- Every walkthrough will have one or more reviewers. Reviewers are relevant stakeholders or knowledgeable non-stakeholders who can ask questions and poke holes in the architecture during the review. On small teams, the recorder and reader might also be reviewers. 

Walkthroughs should be small, with no more than 3--7 participants.

## Materials

- Reviewers should look at the quality attribute scenarios, relevant architecture descriptions, and other background materials as homework prior to the review meeting. If such materials do not exist, prepare an introductory presentation (such as the architecture briefing described) and allow extra time to bring reviewers up to speed. 
- Prioritized quality attribute scenarios must be prepared prior to the start of the meeting.

## Steps

1. Distribute scenarios and architecture artifacts to the group. Set up projectors or screen sharing so the architect can easily share relevant views.
2. The reader picks a quality attribute scenario and reads it out loud. The purpose of reading the whole scenario is to ensure everyone knows the context and general scope of the scenario. 
3. The reader then repeats the stimulus of the quality attribute scenario, thus kicking off the walkthrough. 
4. The architect describes how the system responds to the stimulus, by walking through elements in the architecture. 
5. Once the architect has completed the initial walkthrough, reviewers may ask questions and point out potential architectural issues. 
6. The architect may briefly respond to questions. Issues, risks, and questions raised should be recorded for further analysis by the team after the review meeting.
7. After all reviewers have shared their feedback or time has expired, pick another scenario and repeat the process.

## Tips & Tricks

- Avoid turning the review into a witch hunt. The whole point is to find potential problems while they are still cheap to fix—on paper. Finding issues is a good thing. 
- To keep the meeting moving, the time spent on each scenario should be time boxed. 
- Avoid problem solving during the review. The purpose of this activity is to surface issues, not solve them. 
- The reader and recorder roles can be combined but should be separate from the architect role. 
- Rotate roles to help build teammates’ skills.
- Write or project the current the quality attribute scenario so reviewers can see it during the walkthrough. If this is not practical, distribute a packet with quality attribute scenarios. 
- Record new quality attribute scenarios raised during the walkthrough.

## Example

Let’s walk through an availability scenario from Project Lionheart. A user’s searches for open RFPs and receives a list of available RFPs 99 percent of the time on average over the course of the year. To walk through an availability scenario we’ll need to focus on specific conditions. Recall that Project Lionheart consists of a small handful of web services, a few databases, and a search index.

Here’s a specific quality attribute scenario:

![](assets/scenario-1.png)

The City of Springfield’s IT Department has decided to host Project Lionheart services using a popular cloud provider. Services and processes are hosted in Docker containers in two different cloud regions. This way, when one region fails another is still available.

![](assets/scenario-2.png)

Here is one potential walkthrough for the given quality attribute scenario: 

Reader: 
> The next scenario covers availability during a region failure. Let’s start by assuming everything is up and running. OK, bam! Region A just went down. 
 
Architect: 
> Within 60 seconds, our cross-region load balancer will detect the failure and automatically route traffic to the available region. If a user was unlucky enough to be on the website at that moment, they’ll get a failure. Refreshing the browser should fix the problem.

Reviewer 1: 
> Where is the multiregion load balancer hosted? 

Architect: 
> In the closet across the hall. 

Reviewer 1: 
> So all site accessibility is determined by load balancers we’re managing? Why bother with the cloud platform at all if our weakest link is in the building? 

Reader: 
> Reviewer 1, let’s work to keep our conversation constructive. Can you try to phrase your concern as a risk?

Reviewer 1: 
> Sorry, I was just a bit surprised. How about this: There is a single point-of-failure in our cross-region strategy; might not be able to meet required service-level agreements. (Recorder verifies the concern is captured.)

Reviewer 2: 
> How is the data kept up to date in the proposed multiregion deployment…