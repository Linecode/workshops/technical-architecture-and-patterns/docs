# Divide and Conquer

In cases where you need to cover a lot of ground, break your team into small, independent groups dedicated to exploring a single problem. Divide the problem into smaller chunks and conquer them in parallel. Independent groups can use any exploration method they want within their group, but we still want some oversight to ensure everyone’s time is well spent.

Dividing the exploration space into smaller focus areas increases the risk that you’ll arrive at a fragmented and inconsistent design. The risk is especially high when you accidentally divide the exploration space in a way that prevents groups from exploring independently. You won’t know this until after the groups have started their investigations, so you need to account for this in the method.

Divide and conquer works best with a tight feedback loop in which the broader group attempts to converge thinking regularly. The time between when the exploration space is divided and converges again can be as short as a few hours or as long as a week. Here’s what the divide-and-conquer process looks like:

![](assets/divide.png)

## Benefits

- Explore more of solution space in a shorter period of time. 
- See a range of design ideas that cover similar areas.
- Give designers the time needed to adequately explore solutions. Not every exploration should be time-boxed to a 90-minute workshop. 
- Use a larger group effectively by exploring different areas in parallel.

## Participants

Divide the whole team and technical stakeholders into groups of 2--4. The architect leading the exercise may participate in a group, but it is beneficial for the lead architect to float among groups to resolve issues and offer coaching.

## Materials

- Before starting the exercise, you need to know enough about the lay of the land to divide the solution space for exploration. Create a prioritized list of open questions and risks to drive the planning. 
- Decide how you will record group commitments. Prepare slides or notes for a kickoff meeting, which all participants will attend.

## Steps

- Hold a kickoff meeting. Explain the ground rules of the exploration and set expectations for what each group will share when the group reconvenes. The most important rule is that everyone shares what they have, whatever it is, when the groups reconvene. 
- Divide the exploration space and help participants self-organize into groups of 2--4 people. We do these things at the same time so participants can better adapt to an evolving situation. By the end of this stage, every participant should be in a group and every group should have a clear mission. 
  When dividing the exploration space you may take either a breadth-first or a depth-first approach. In the depth-first approach, all groups explore the same general area. In the breadth-first approach, every group explores something different. Go depth first when you are confident in a general solution and need to refine it. Prefer breadth first when you need to quickly reduce risks across a variety of topics.
- Establish the due date. Schedule the show-and-tell meeting. By scheduling the show-and-tell meeting during the kickoff, you create a social contract with each group. Everyone is expected to share something at the show-and-tell meeting. 
- Record each group’s commitments for the exploration. This is the exploration plan. The general idea is to set clear expectations for what each group will accomplish during the exploration. At the show-and-tell meeting, groups are expected to show what they committed to exploring during the kickoff, or explain what prevented them from achieving their goals. 
- Begin the exploration. Groups divide and explore as they see fit. The architect should check on groups as they work. 
- Reconvene for the show-and-tell meeting at the agreed place and time. During the meeting each group shows their accomplishments relative to their exploration goals and briefly tells what they learned. Participants from other groups should have time to ask questions and provide constructive criticism. Note any new questions or risks raised during the show-and-tell meeting. 
- If there is more to explore, immediately plan another iteration of exploration. Go to step 2.

## Tips & Tricks

- All groups must share during the show-and-tell meeting. If the team completely missed their exploration goals, use this as a coachable moment to pivot or realign the group. 
- To maximize exploration potential, encourage people to form cross-functional groups with people they don’t work with every day. Consider occasionally mixing groups. 
- Keep the groups small to avoid gold plating the designs and bike-shedding discussions in which the group focuses on trivial, tangential matters. 
- Commitments made during exploration planning should come from the group. Some groups will need help scoping their commitments appropriately to the available exploration time. 
- Remind groups that the exploration phase is ending so that they have enough time to prepare for the show-and-tell meeting.

## Example

After first week:

| Group | Exploration Plan                                                                   | Show and Tell                                                                                   |
| ----- | ---------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| One   | Refactor plug-in framework to see if it's possible to extract from legacy codebase | Showed primary interfaces and classes for the refactoring which demonstrates the plan i feaible |
| Two   | Hello worl gRPC web service                                                        | Demo of a Ruby-based client talking to a service implemented in Java                            |
| Three | Create a concept map and draft microservice partioning                             | Draft concept map. Feedback from the group indicated that more work was needed                  |

After second week:

| Group | Exploration Plan                                   | Show and Tell                                                                             |
| ----- | -------------------------------------------------- | ----------------------------------------------------------------------------------------- |
| One   | Command-line invocation of legacy plug-ins         | This turned out to be more work than expected. Described raodblocks and remediation plans |
| Two   | Recommend database technology                      | Three demon of different database technologies with a quick peek at the code              |
| Three | Revise concept map, draft microservice partioning. | Concept map and microservice overview.                                                    |

