# Whiteboard Jam

Collaboratively draw a series of diagrams that best capture the whole group’s ideas. We’ve all done this before. Gather some teammates around a whiteboard, put a marker in everyone’s hand, and start sketching. The activity described here adds a smidge of structure to something most architects do naturally. This additional structure helps the activity become more consistent and encourages better outcomes.

## Benefits

- Help opinionated teams get their ideas out in the open. 
- Quickly move through design alternatives by forgoing formality and immediately improving ideas based on feedback.
- Create a shared cultural experience upon which further design insights can be created. 
- Include many participants in the discussion. 
- Facilitate the activity as a participant.

## Participants

Any technical stakeholder may participate. The number of active participants is limited by the amount of whiteboard space, though 3--5 participants seems to be a nice sweet spot. Participants will sometimes come and go throughout the session.

## Materials

Start with a clean whiteboard and have plenty of markers of different colors.

## Steps

- Set the stage by reviewing the objectives for the whiteboard jam. Write the objectives on the whiteboard so everyone can see them. 
- Encourage someone to sketch his or her ideas on the whiteboard. 
- Take turns describing your sketches to the group. As you are describing, it’s OK for others to start sketching new ideas, riffing on your work. 
- After the initial sketches are up, briefly critique the designs. Write issues that must be addressed on the whiteboard.
- Take turns adjusting the sketches already on the whiteboard or drawing new ones. 
- Continue adjusting sketches, sharing updates together, and critiquing until time runs out, all ideas are covered, the group reaches consensus, or the group reaches an impasse. 
- Take pictures of the whiteboard and summarize the results in your team wiki along with a brief write-up of the discussion.

## Tips & Tricks

- Use during impromptu discussions to resolve confusion and capture multiple ideas under discussion. 
- Write down important discussion points on the whiteboard as they are raised during the jam.
- Occasionally pause to reflect on the sketches and ask questions. Most jams follow a natural create-share-critique flow described. 
- Encourage everyone to draw. It’s OK to sketch while others are talking. 
- The diagrams themselves are only useful as a cultural artifact for the people who participated in the jam. Pictures will jog participants’ memories but won’t make sense to someone who wasn’t there. The discussions take place during the whiteboard jam are often more important than the sketches.


## Example

![](assets/whiteboard-jam.png)
