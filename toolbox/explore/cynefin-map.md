# Cynefin Map

The Cynefin framework, developed by Dave Snowden, is a decision-making tool that helps individuals and organizations determine the complexity of a given situation or problem and choose the most appropriate approach to deal with it. The framework categorizes problems into five domains: Clear (formerly Simple), Complicated, Complex, Chaotic, and Disorder.

## Benefits

1. **Enhanced Decision Making**: Provides clarity on how to approach different problems based on their complexity.
2. **Risk Management**: Helps in understanding the inherent risks in different domains.
3. **Adaptable Strategy**: Allows organizations to adapt their approach as situations evolve.
4. **Improved Understanding**: Differentiates between situations that can be analyzed and those that need emergent practices.
5. **Resource Optimization**: Ensures efforts and resources are directed most effectively based on the nature of the problem.

## Participants

1. **Leaders & Decision Makers**: Use the framework to guide strategic decisions.
2. **Project Managers**: Can adapt project methodologies based on the problem's domain.
3. **Consultants**: Offer insights and guide organizations through the decision-making process.
4. **Teams**: Understand the context of the tasks they are working on.
5. **Stakeholders**: Gain clarity on organizational approaches and strategies.

## Materials

1. **Cynefin Diagram**: A visual representation of the framework.
2. **Whiteboard or Large Paper**: To draw out the framework and discuss specific problems.
3. **Markers**: For annotating the diagram and adding details.
4. **Sticky Notes**: Useful for placing different problems or scenarios on the framework.
5. **Digital Tools**: Tools like Miro or Mural can be valuable for remote discussions.

## Steps

1. **Understand the Domains**: Familiarize participants with the five domains of the Cynefin framework.
2. **Categorize Problems/Scenarios**: Identify current challenges or decisions and categorize them into the respective domains.
3. **Determine Appropriate Actions**: For each domain, identify the recommended actions or methodologies.
4. **Iterate & Re-evaluate**: Continually assess the situation, as problems can shift between domains.
5. **Document Decisions**: Record decisions made and strategies employed for future reference.
6. **Review & Adjust**: As strategies are implemented, review their effectiveness and adjust as necessary.

## Tips & Tricks

1. **Stay Open-minded**: The true value of Cynefin is in its ability to challenge traditional ways of thinking.
2. **Continuous Assessment**: As situations evolve, they may shift from one domain to another. Continual reassessment is key.
3. **Use Real-world Examples**: When explaining the framework, real-world examples can help in understanding.
4. **Seek Diverse Perspectives**: Different individuals may interpret a situation differently. Having a range of perspectives can lead to a richer understanding.
5. **Avoid Overcomplication**: While Cynefin delves into complexity, the aim should always be to clarify and not complicate decisions.

## Example

Imagine an organization is facing the challenge of a significant decline in sales:

- If the cause is clear, like a price increase (Clear domain), then the response might be to revert to the old pricing.
- If the reason involves a series of factors, like market conditions, competitor strategies, and product positioning (Complicated domain), then a detailed analysis may be required.
- If the sales decline is due to unpredictable customer behaviors and rapidly changing market trends (Complex domain), then the organization might need to experiment with various strategies and learn from emergent patterns.
- If there's a sudden market crash or an unforeseen external event causing the decline (Chaotic domain), immediate crisis management and containment actions would be needed.

By utilizing the Cynefin framework, organizations can more effectively navigate the complexities of their environment, making informed and contextually appropriate decisions.