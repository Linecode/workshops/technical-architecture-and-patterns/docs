# Architecture Flipbook

In an architecture flipbook, we record every step of the design journey so others can follow along afterward. Every page of the flipbook includes a sketch and notes about incremental changes to a model. We use this record to think through options or backtrack to an earlier decision that might have led us astray. As a bonus, the resulting flipbook explains why the architecture looks the way it does. 

Most people only see the final results of your design toils. All the wrong turns, goof-ups, and critical aha! moments become the designer’s secret memories of their personal journey exploring design ideas. It’s a shame these moments are lost since we can learn a lot by peeking into the architect’s mind and seeing a model as it evolves.


## Benefits

- Methodically think through a model. 
- Externalize the branching and backtracking that happens naturally during design. 
- Teach others how to think about design and modeling. 
- Remove some of the mystery as to where the ideas for a model come from.

## Participants

This activity can be completed alone or as a small group of 2--3 people.

## Materials

Choose a simple diagramming tool you like to use. Microsoft PowerPoint or similar works well for this exercise. You can also draw on paper and take pictures as you go.

## Steps

- Pick a user story or quality attribute scenario to use as the motivation for the model you’ll create. 
- On the first slide, describe the problem and any architecturally significant requirements relevant to the model you want to explore. 
- On the next slide, brainstorm and record interesting domain concepts from the problem and briefly describe them.
- The next slide starts blank since we haven’t created a model yet. Add a single element that you think is in the solution space for the system. 
- Copy the slide you just created and try to apply the user scenario or story you picked at the beginning. Can you achieve the scenario? Do any new questions arise that are specific to the solution space? Write down your questions and comments. Choose one thing to address and amend the model by adding a new element and required relations. 
- Repeat step 5 until you can successfully complete the user story or scenario, and all open questions have been answered. If you get stuck, backtrack to an earlier model and continue from that point. Indicate that the new slide is a branch of an earlier point.
- Review the flipbook for inconsistencies and key moments. Use the history to help summarize the rationale for the final model.

## Tips & Tricks

- Start with the obvious concepts. 
- Watch out for implied concepts or completely new concepts that are not explicitly named in the problem domain. These hidden concepts are the among the most interesting and important to get right. 
- Make small changes with each step in the flipbook. 
- Look for inconsistencies in the model relative to the scenario or user story to build the model.

## Example

Here is the influential functional requirement used to seed the flipbook: 

> A trainer user can add queries with document references so that she can train a new predictive model.

![](assets/architecture-flipbook.png)