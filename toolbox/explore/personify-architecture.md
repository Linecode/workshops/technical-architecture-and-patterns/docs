# Personify The Architecture

To personify the architecture means to give it human qualities so that you can explore interactions among elements. Talking about the architecture as if it had human emotions helps us apply our experience with human relationships when designing the architecture. To use this technique, pretend elements in the architecture are people or animals and describe their emotions, motivations, goals, and reactions to stimuli. Anthropomorphism is a fun, natural way to explore design concepts. 

Anthropomorphism is also problematic since applying fictional human qualities to a software system is imprecise and ambiguous. We’re making up a story about our software and projecting human-like qualities onto the system! Trading precision for effective communication so that the architecture is easier to explain is usually worth it.

## Benefits

- Make the architecture more relatable. 
- Qualify desirable and undesirable properties and situations by thinking of how elements will “react” or “feel.” 
- Create memorable stories that help the team keep architectural concerns at the center of design conversations. 
- Increase buy-in of design decisions by making the architecture almost feel like a teammate. 
- Quickly try different emotions and reactions through simple story telling.

## Participants

No preparation is required. This technique is often used during impromptu conversations about the architecture.

## Materials

N/A

## Steps

- Pick a piece of the architecture for which you need to describe behavior. Think about a quality attribute scenario or functional requirement that element must satisfy. 
- Pretend the architecture has human qualities. How would the elements respond to the stimulus in the quality attribute scenario or functional requirement? Tell a story about what the elements do. Describe their motivation and reactions as if they were people.
- Try different reactions and emotions for the same set of elements under discussion. Introduce variations into the architecture to see how elements might need to change their behavior. 
- After exploring different ideas, codify the options that look promising. Create a system metaphor, sketches, or other documentation for further analysis.

## Tips & Tricks

- Use anthropomorphism as a part of your team’s regular design discussions. 
- It’s OK to feel a little silly. We are pretending the architecture is human, after all. 
- Accompany stories with sketches to make the ideas discussed more concrete.
- Anthropomorphism is not a substitute for architectural views that describe the system using more precise language.

## Example

> Our services are *fickle*. They don’t care where they live or which service instances they talk to from one request to the next. 

> Most of our *services* are *stubborn*. They retry requests when the first request fails. 

> Some of our services are *moody* and *impatient*. Moody services give up if they can’t get what they want quickly enough. They’ll resentfully make do with the data they have.

> Some of our services are best buds. They chat to each other often. We even discussed deploying them together in pairs so they won’t be lonely.
