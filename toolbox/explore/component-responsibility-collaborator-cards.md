# CRC Cards

## Benefits

- Quickly Iterate through design alternatives
- Create group buy-in and shared understanding of the architecture
- Create a connection between architecturally significant requirements and design alternatives
- Identify potential gaps in architecture

## Participants

- Development team (3-5 people)
- Solo exercise

## Materials

- Index cards and markers, or miro board
- Collect some of your's system functional requirements (use cases, stories, or similar) and quality attributes scenarios

## Steps

- Introduce goals
- Read aloud a functional requirements or quality attribute scenario
- Create a card to represent the user or source for a quality attribute scenario
- Add a new card to the table to represent the architectural element with which the trigger first interacts.
- Evolve the architecture by adding cards for known elements or creating new elements as needed.
- As desgin alternatives emerge, keep all the cards on the table. Move cards to the side in case they are needed later.
- Pick a new functional requirement or quality attribute scenario and walk through the architecture again
- Repeat previous 4 steps until times run out or functional / non-functional requirements runs out
- Record the elements and their assigned responsibilities
- Record key decisions and design principles

## Tips & Tricks

- Use sticky notes to represent elements
- Keep the exercise fun and fresh by drawing pictures, not only text
- It's ok to informally mix structures (static, dynamic and physical) if it helps with reasoning
- Use digital collaboration tool
- Every card should have at least one responsibility by the end of the activity. 

## Example

![](assets/toolkit-crc-cards.png)