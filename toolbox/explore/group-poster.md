# Group Posters

Small groups work together to create a poster that conveys their design ideas for the architecture. This activity is well suited to summarizing outcomes from other workshops.

## Benefits

- Produce several alternative models for comparison. 
- Build pockets of consensus and spread knowledge within a larger group of stakeholders. 
- Create artifacts that can be easily shared with people outside the group. 
- Quickly explore and summarize architecture design ideas.

## Participants

Stakeholders work in groups of 2--5 people. Stakeholders who work together regularly should be in different groups.

## Materials

- Flipchart paper and markers

## Steps

- If needed, review architecture sketching basics. 
- Review the goals for the activity. All participants will produce a poster that solves the same problem.
- Divide participants into groups or allow them to self-organize. Distribute flipchart paper and markers. 
- Groups create a common vision for the architecture within the scope of the agreed goals. 
- When time expires, each group shares their poster. Give each group 3 minutes to present their poster. Questions and comments should be held until after the presentation. 
- Allow 3--5 minutes to critique the poster after each presentation. 
- Once all posters have been shared, briefly discuss any trends or general observations about the posters together. 
- Initiate a round of dot voting. Given each participant 1 vote for best overall poster and 3 votes for interesting design ideas that appear on any poster. Discuss the outcomes of the voting.

## Tips & Tricks

- Remind participants to include a legend and think about which views of the architecture they are sketching—module, component and connector, or allocation. 
- It’s OK to sketch more than just structures. Sketches of domain models, sequence diagrams, or state diagrams can all be useful. 
- Encourage participants to jot down open questions or risks that arise during their group discussions. 
- Monitor participant progress closely and adjust time up or down to ensure groups are creating effective posters.
- During the critiques, remind participants to focus on facts and avoid “I like…” kinds of remarks. 
- Record video or audio of the poster presentations for later review. 
- Keep the posters and hang them in your workspace.

## Example