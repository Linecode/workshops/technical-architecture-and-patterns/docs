Event storming is a collaborative brainstorming technique used to identify domain events. Event storming can be used as a precursor to more in-depth domain modeling exercises, to assess the team’s current understanding of the domain, and to identify risks and open questions in an existing domain model. Event storming is described fully in Introducing Event Storming: An Act of Deliberate Collective Learning  by Alberto Brandolini. 

Event storming helps teams better engage with subject matter experts who are knowledgeable about the domain but may have trouble pairing with developers directly. Event storming accomplishes this in two ways.

First, the format requires active engagement from all participants. Subject matter experts have no choice but to inject their knowledge into the process. Second, event storming encourages participants to be concrete and specific. If you have subject matter experts working with you, encourage them to describe their jobs and expertise in gory detail.

## Benefits

- Visualize learning opportunities and facilitate a structured conversation about the domain. 
- Uncover assumptions about how people think the system works. This allows you to identify misunderstandings and missing concepts. 
- Create a shared understanding of the problem and potential solutions. 
- Produce a highly visual, tactile representation of business concepts and how they relate. 
- Enable diverse viewpoints to be represented.
- Allow participants to quickly try out multiple domain models so they can see where those concepts work and where they break down. 
- Focus on concrete examples, not abstract ideas.

## Participants

Subject matter experts knowledgeable in the problem domain must participate. A few members of the development team should also participate to take advantage of the learning opportunity. If knowledgeable subject matter experts are not available, the workshop may not have great outcomes. This workshop can be run with as few as 2--3 participants and as many as a dozen or more, depending on the workspace and facilitator’s experience.

## Steps
Proces pozyskiwania rzeczy  
  
Proces:  
 

-   Szybki
-   Stopniowy
-   Uwidacznianie braków
-   Synchronizuje wiedzę
-   Prosty (szczególnie dla biznesu, niski próg wejścia)

Kogo zaprosić:

-   Stakeholderzy kluczowi, istotni

Przygotowanie:

-   Przestrzeń
-   Ściany
-   Karteczki i flamastry
-   Flipchart
-   Minutnik

## Rozkład Warsztatów
### Faza 0:  Przywitanie, Cel  
-   Przywitanie, mały round table kto jest kim i za co jest odpowiedzialny w tym projekcie
-   Przedstawienie celów

-   Zdefiniowanie procesów w zakresie MVP
-   Wizualizacja niejasności
-   Określenie zakresu MVP

### Faza 1: Chaotyczna eksploracja

-   Wdrażamy koncept zdarzeń

-   Coś ważnego co wydarzyło się w systemie
-   Czas przeszły
-   Wyrażenia nie musza event-like w tej części

-   Zacznijmy od prostego przykładu: "zamawianie pizzy"

Przydatne info

-   4 poziomy zdarzeń

-   UI
-   ENV
-   Domain -> te chcemy poznać, dwie wcześniejsze mogą być dla złapania kontekstu
-   Infra

-   Przestań czepiać się "phrase" w tej części w końcu!!

### Faza 2: Timeline

-   Wymuszamy timeline
-   Szukamy autonomicznych części procesu
-   Sortowanie za pomocą:

-   Pivotal Event
-   Swimmlines

-   HotSpots!!

### Faza 3: Ludzie i systemy

-   Dodanie aktorów i system-ów z którymi komunikuje się proces
-   Extra Step (Adding value: kasa, zaufanie, stres, szczescie)

### Faza 4: Narriacja

-   Walk-through
-   Reverse Narriation

### Faza 5: Problemy i Okazje

### Faza 6 : Wrapping up Big Picture

  
  
### Faza 7:  Process Level ES

-   Dodajemy komendy (Akcje)
-   Eventy muszą być przepisane na "zmiany stanów"
-   Dodawanie polityk, polityka to brakujący element pomiędzy eventem a akcją
-   Read Models - dane potrzebne aktorowi do przeprowadzenia akcji

![](assets/event-storming.png)
