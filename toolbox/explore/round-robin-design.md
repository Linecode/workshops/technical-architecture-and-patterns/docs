# Round Robin Design

Quickly explore a range of ideas, and then combine them to start building consensus. In a round-robin design workshop, participants quickly generate, share, and critique sketches of the architecture to help them see a range of possibilities. By the end of the activity, participants will have seen at least two new ideas in addition to their own. 

Participants go through three rounds. In round one, participants generate a sketch. In round two, participants review someone else’s sketch. In round three, participants attempt to fix the issues raised in a third person’s sketch. 

Use this activity as a sanity check exercise (see Activity 36, ​Sanity Check​) or to set the stage for a follow-on activity such as creating group posters, described.

![](assets/round-robin-design.png)

## Benefits

- Give everyone a voice and opportunity to share their design ideas. 
- Foster creativity by constraining the design environment. 
- Create opportunities for unintended combinations. Encourage group ownership of the design.  
- Build consensus among possibly disparate ideas.
- Expose differences (and similarities) in thinking across the group.

## Participants

Since we’re sketching architectural models, this activity is usually best reserved for technical stakeholders. At least 3 people are required to participate. Conversations start to break down with more than about a dozen participants.

## Materials

Standard-sized paper Pens or markers of three different colors

## Steps

- Distribute pens and paper to the group.
- Agree on the exploration goal—a specific view, quality attribute, or type of model (for example, API or domain model). 
- All participants sketch for 5 minutes. Encourage unconventional ideas. 
- Pass your sketches to the person on the left. 
- Using a different colored pen, critique the sketch for 3 minutes. Add annotations directly to the paper. 
- Again, pass your sketches to the person on the left. 
- Using a different colored pen, improve the design to overcome weaknesses identified by the critique for 5 minutes. 
- Pass the papers back to the original designer. Review the sketches as a group and briefly discuss.

## Tips & Tricks

- Do not disclose all steps at the beginning of the activity. Hold paper trading as a surprise. 
- Informal views are fine during the sketches. 
- Encourage participants to use any notations needed to convey an idea.

## Example

![](assets/round-robin-design-2.png)