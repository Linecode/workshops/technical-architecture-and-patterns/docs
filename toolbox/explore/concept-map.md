# Concept Map

A concept map is a method for exploring domain concepts by visualizing how concepts in the domain are related to one another. Great software architectures are grounded in the problem domain. Concept maps help us uncover specific ideas from the problem domain as well as implicit ideas required to implement a solution. Every domain concept needs a home in the architecture. The relationships among domain concepts can help us pick the right patterns, interaction models, and information architectures.

## Benefits

- Visualize domain concepts and their relationships. 
- Try out different relationships among domain concepts.
- Uncover missing, hidden, or implied domain concepts required to implement a functional software system. 
- Lay a foundation for partitioning architectural elements and for defining potential relations among elements. 
- Provide a resource for evaluating an architecture’s fitness. Is it consistent with the domain model? 
- Outline a domain rich vocabulary for the software system.

## Participants

Create a concept map with technical stakeholders. Work alone or as a small group of 2--3 people.
Verify a concept map with knowledgeable stakeholders.

## Materials

Use drawing software for a digital map, or paper and pencil if you’re going analog.

## Steps

- Choose a starting concept from the problem domain to seed the map. This will usually be a prominent noun from an architecturally significant requirement. Write the concept’s name down and draw a box around it. 
- Record related concepts and connect them to each other as appropriate. Determine the cardinality of each relationship. Give each relationship a specific name. Relations should read like a sentence—Concept A does something to or with Concept B.
- Choose a functional requirement or quality attribute scenario to help flesh out the domain concepts. Attempt to describe how the scenario would be satisfied by your current domain concepts. Pay close attention to concept gaps and omniscient concepts. A concept gap occurs when ideas are missing from the domain model. You’ll know when this happens because you will not be able to complete a scenario without introducing new concepts. An omniscient concept is one that magically seems to know everything it needs to connect to other, potentially unrelated concepts. Identifying omniscient concepts requires a high degree of introspection over the domain and concept map.
- Revise the concept map to introduce newly uncovered concepts. Repeat step 3 until the scenario can be fully satisfied. 
- Pick a new scenario. Refine the concept map as needed to address the new scenario.

## Tips & Tricks

- Use boxes to represent concepts. Use lines to show how concepts are related. 
- Be specific when naming concepts and describing how they are related. See Belshee’s 7 Stages of Naming for naming advice. 
- Label both ends of a concept relationship. 
- Be prepared for concepts and relations to move as the map emerges.

## Example

This example concept map from Project Lionheart shows several core concepts from the domain. The map reads, City Department issues zero or many RFPs while an RFP describes the needs of one or more City Departments.

![](assets/concept-map.png)

## Notes

Very similar to Context mapping from DDD