# Toolbox

## Understand

- [Agile Personas](understand/agile-personas.md)
- [Back of the envelope estimations](understand/back-of-the-envelope-estimation.md)
- [Choose one Thing](understand/choose-one-thing.md)
- [Competency Matrix](understand/competency-matrix.md)
- [Empathy Map](understand/empathy-map.md)
- [Goal Question Metric](understand/goal-question-metric.md)
- [Impact Mapping](understand/impact-mapping.md)
- [Interview Stakeholders](understand/interview-stakeholders.md)
- [List Assumptions](understand/list-assumptions.md)
- [Mini Quality Attribtue Workshop](understand/mini-quality-attribute-workshop.md)
- [Point of View Mad Lib](understand/point-of-view-mad-lib.md)
- [Quality Attribute Web](understand/quality-attribute-web.md)
- [Response Measure Straw Man](understand/response-measure-straw-man.md)
- [Stakeholder Map](understand/stakeholder-map.md)

## Make

- [ADR](make/architecture-decision-record.md)
- [Architecture Haiku](make/architecture-haiku.md)
- [Context Diagram](make/context-diagram.md)
- [Greatest Hits Reading List](make/greatest-hits-reading-list.md)
- [Inception Deck](make/inception-deck.md)
- [Modular Decomposition diagram](make/modular-decomposition-diagram.md)
- [Paths Not Taken](make/path-not-taken.md)
- [Prototype](make/prototype.md)
- [Sequence Diagram](make/sequence-diagram.md)
- [System Methaphor](make/system-metaphor.md)

## Explore

- [Architecture Flipbook](explore/architecture-flipbook.md)
- [CRC Cards](explore/component-responsibility-collaborator-cards.md)
- [Concept Map](explore/concept-map.md)
- [Cynefin](explore/cynefin-map.md)
- [Divide and Conquer](explore/divide-and-conquer.md)
- [Domain Storytelling](explore/domain-storytelling.md)
- [Event Storming](explore/event-storming.md)
- [Group Poster](explore/group-poster.md)
- [Personify Architecture](explore/personify-architecture.md)
- [Round Robin Design](explore/round-robin-design.md)
- [User Story Mapping](explore/user-story-mapping.md)
- [Whiteboard Jam](explore/whiteboard-jam.md)

## Evaluate

- [Architecture Briefing](evaluate/architecture-briefing.md)
- [Code Review](evaluate/code-review.md)
- [Decision Matrix](evaluate/decision-matrix.md)
- [One Door vs Two Doors](evaluate/one-door-two-doors-decisions.md)
- [Question-Comment-Concern](evaluate/question-comment-concern.md)
- [Risk Storming](evaluate/risk-storming.md)
- [Sanity Check](evaluate/sanity-check.md)
- [Scenario Walkthrough](evaluate/scenario-walkthrough.md)
- [Sketch and Compare](evaluate/sketch-and-compare.md)