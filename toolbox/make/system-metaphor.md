# System Metaphor

Tell a simple story that demonstrates how the system influences specific quality attributes. The system metaphor was introduced by Kent Beck in Extreme Programming Explained: Embrace Change  as a way to create a common vision and shared vocabulary for the architecture.

## Benefits

- Lightweight description technique perfect for co-located teams to use during times of fast architectural evolution 
- Can be combined with other description methods 
- Cheap to create, easy to change

## Description

In Making Metaphors That Matter, Michail Velichansky and I summarize concrete guidance for creating useful system metaphors. Good system metaphors have the following attributes: 

- Represent a single view of the system. 
- Deal with only one type of structure.  
- Provide clear guidance concerning design decisions.  
- Shed light on system properties.  
- Draw on a shared experience. 
- Corollary: Even a good metaphor still requires explanation. 

Every system metaphor comes with an information payload—the discussions and diagrams that went into creating the metaphor. The metaphor becomes a reference point to this other information and is meant to help team members recall these important details.

## Tips & Tricks

- Tell a memorable story and have fun.
- Be specific and focus on what makes your system unique. Every software system ever made is like a city. 
- If a common reference point does not exist, then create a shared experience. 
- Pop culture and food are common points of reference for many metaphors. 
- Common architecture patterns serve the same purpose as system metaphors and can be used in the same way.

## Example