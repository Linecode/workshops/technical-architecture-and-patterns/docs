# Architecture Decision Record

Capture architecture design decisions as they are made using a lightweight, text-based template. Lightweight decision records are a developer-friendly approach to a time-tested architecture practice. Documenting design decisions makes it easier to share and analyze them. Retaining a history of decisions provides context for the current architecture relative to its evolution.

## Benefits

- Make recording design decisions a team responsibility. 
- Keep key decisions close to the code by storing them in the code repository. 
- Combine with other artifacts to create a holistic description strategy. 
- Capture history to gain perspective on the evolution of the design.
- Involve the whole team in the design process. 
- Train teammates in architectural thinking by providing ADR templates.  
- Enable peer review of design decisions using standard development tools and an existing peer review workflow.

## Tips & Tricks

- Include only one decision per file. 
- Sequentially number ADRs and keep old records. Add references to old records when a decision is superseded or changed. 
- Keep ADRs short, one or two pages at most. Use plain language when recording decisions. 
- Put architecture decisions through the same review process as code. 
- Store in version control with other code artifacts. 
- ADRs should not be the only architecture documentation you create. Combine with other artifacts such as views, architecture haikus, and system metaphors.

## Example

### Michael Nygard

```
# Decision record template by Michael Nygard

This is the template in [Documenting architecture decisions - Michael Nygard](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).
You can use [adr-tools](https://github.com/npryce/adr-tools) for managing the ADR files.

In each ADR file, write these sections:

# Title

## Status

What is the status, such as proposed, accepted, rejected, deprecated, superseded, etc.?

## Context

What is the issue that we're seeing that is motivating this decision or change?

## Decision

What is the change that we're proposing and/or doing?

## Consequences

What becomes easier or more difficult to do because of this change?
```

### MADR

```
# [short title of solved problem and solution]

* Status: [proposed | rejected | accepted | deprecated | … | superseded by [ADR-0005](0005-example.md)] <!-- optional -->
* Deciders: [list everyone involved in the decision] <!-- optional -->
* Date: [YYYY-MM-DD when the decision was last updated] <!-- optional -->

Technical Story: [description | ticket/issue URL] <!-- optional -->

## Context and Problem Statement

[Describe the context and problem statement, e.g., in free form using two to three sentences. You may want to articulate the problem in form of a question.]

## Decision Drivers <!-- optional -->

* [driver 1, e.g., a force, facing concern, …]
* [driver 2, e.g., a force, facing concern, …]
* … <!-- numbers of drivers can vary -->

## Considered Options

* [option 1]
* [option 2]
* [option 3]
* … <!-- numbers of options can vary -->

## Decision Outcome

Chosen option: "[option 1]", because [justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | … | comes out best (see below)].

### Positive Consequences <!-- optional -->

* [e.g., improvement of quality attribute satisfaction, follow-up decisions required, …]
* …

### Negative Consequences <!-- optional -->

* [e.g., compromising quality attribute, follow-up decisions required, …]
* …

## Pros and Cons of the Options <!-- optional -->

### [option 1]

[example | description | pointer to more information | …] <!-- optional -->

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c]
* … <!-- numbers of pros and cons can vary -->

### [option 2]

[example | description | pointer to more information | …] <!-- optional -->

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c]
* … <!-- numbers of pros and cons can vary -->

### [option 3]

[example | description | pointer to more information | …] <!-- optional -->

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c]
* … <!-- numbers of pros and cons can vary -->

## Links <!-- optional -->

* [Link type] [Link to ADR] <!-- example: Refined by [ADR-0005](0005-example.md) -->
* … <!-- numbers of links can vary -->
```

## RFC Like Process