# Sequence Diagram

Dynamic structures are difficult to appreciate on paper. Use a sequence diagram to show how control or data moves through the system at runtime. Model the flow of logic, data, or control among components.

## Benefits

- Simple and flexible notation  
- Both graphical and text notations exist 
- Useful for communication and reasoning 
- Ample tool support, though tools are not required

## Description

1. Choose a scenario to diagram. Use this as the title of the diagram.
2. List components involved in the scenario horizontally across the top of the page. These are the participants in the diagram. 
3. Draw a straight, vertical life line under each participant. Participants are usually listed from left to right starting with the participant that initiates the scenario. Draw arrows from one participant’s life line to another to indicate communication between those components. Label the line to describe the message. 
4. Time goes down the y-axis in the diagram. Since the next message happens after the first, it should be further down the y-axis.

## Tips & Tricks

- Reason about distributed systems, microservices, object communication, and other dynamic structures. 
- Informal notations are fine as long as you are consistent. 
- A closed arrow with a solid line indicates a synchronous request message. 
- An open arrow with a solid line indicates an asynchronous request message.
- An arrow with a dotted line indicates a response message. 
- Use a tool that renders text-based notations so you can store the diagrams with your code.

## Example

```mermaid
sequenceDiagram 
  Alice->>John: Hello John, how are you? 
  John-->>Alice: Great! 
  Alice-)John: See you later!
```
