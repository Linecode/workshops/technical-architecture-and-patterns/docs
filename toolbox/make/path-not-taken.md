# Paths Not Taken

Create a list of the architectural decisions you discarded with a brief note explaining why you ultimately rejected each decision. Recording the decisions you discarded, the paths not taken, provides context and rationale for a design decision.

## Benefits

- Help downstream designers replay the thought process that went into the current design. 
- Head off did you consider...? discussions with stakeholders. 
- Provide an additional layer of rationale for design decisions.

## Description

List design decisions you considered but rejected along with the reason for why the decision was not selected. The list can be stored in plain text or other easily accessible format.

## Tips & Tricks

- Focus on a single view or design decision. The list should not attempt to encompass all design decisions. 
- Keep it brief. Include only enough detail so that stakeholders can understand the decision and why it was rejected. 
- Combine with other methods to create a more complete description of the architecture. You can list the paths not taken alongside [[Architecture Decision Records]] and [[Architecture Haiku]].

## Example

| Path Not Taken                                                                            | Reason                                                                                                                     |
| ----------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Create a cloud-based "services adapter" to buffer against changes in third-party services | Heavy maintenance costs, benefits in features and quality attribtues not required for MVP release, costs outweigh benefits |

